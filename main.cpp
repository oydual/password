#include <iostream>
#include <password.h>

using namespace std;

int main()
{
    srand(time(NULL));
    int n;
    int longitud;
    password *listaPassword;
    bool *fortaleza;
    cout << "Cantidad de Passwords a Generar: " << endl;
    cin >> n;
    cout << "Longitud deseada pra los Passwords" << endl;
    cin >> longitud;
    listaPassword = new password[n];
    fortaleza =  new bool[n];
    cout << setw(40) << left << "Password" << "Fortaleza" << endl;
    for(int i=0;i<n;i++){
              password aux(longitud);
              listaPassword[i]= aux;
              fortaleza[i]=listaPassword[i].esFuerte();
              cout << setw(40) << left << listaPassword[i].getContrasenia() << fortaleza[i] << endl;
          }
	delete[] listaPassword;
	delete[] fortaleza;
    return 0;
}

