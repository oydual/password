#ifndef PASSWORD_H
#define PASSWORD_H
#define LONG_DEF 8  //Constante que define el tamanio del password

#include<iomanip>
#include<ctime>
#include<cstdlib>
#include<string>

using namespace std;
class password
{
private:
    int longitud;
    string contrasenia;
public:
    //Constructor
    password();
    password(int);

    //Destructor
    ~password();

    //Metodos
    //Setters
    void setLongitud(int);
    //Getters
    int getLongitud();
    string getContrasenia();
    //Operaciones
    string generaContrasenia();
    bool esFuerte();

};

#endif // PASSWORD_H
