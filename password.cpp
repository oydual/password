#include "password.h"

password::password() : contrasenia("")
{
    this->longitud = LONG_DEF;
}
password::password(int L)
{
    if(L > 8){
        this->longitud = L;
    }else{
        this->longitud = LONG_DEF;
    }
    this->contrasenia = generaContrasenia();
}
password::~password(){}

string password::getContrasenia(){
    return this->contrasenia;
}

int password::getLongitud(){
    return this->longitud;
}

string password::generaContrasenia(){
    string password="";
            for (int i=0;i<longitud;i++){
                /*
                 * 1 - Minuscula
                 * 2 - Mayuscula
                 * 3 - Numero
                 */

                int tipo = 1+ (rand()%3);
                if (tipo==1){
                    char minusculas=(char)(97+rand()%26);
                    password+=minusculas;
                }else{
                    if(tipo==2){
                        char mayusculas=(char)(65+rand()%26);
                        password+=mayusculas;
                    }else{
                        char numeros=(char)(48+rand()%10);
                        password+=numeros;
                    }
                }
            }
            return password;
}
bool password::esFuerte(){
    int cuentanumeros=0;
    int cuentaminusculas=0;
    int cuentamayusculas=0;
    //Vamos caracter a caracter y comprobamos que tipo de caracter es
    for (int i=0;i< (int)contrasenia.length();i++){
            if (contrasenia[i]>=97 && contrasenia[i]<=122){
                cuentaminusculas+=1;
            }else{
                if (contrasenia[i]>=65 && contrasenia[i]<=90){
                    cuentamayusculas+=1;
            }else{
                cuentanumeros+=1;
                }
            }
        }
        //Si la constraseña tiene mas de 5 numeros, mas de 1 minuscula y mas de 2 mayusculas
        if (cuentanumeros>=5 && cuentaminusculas>=1 && cuentamayusculas>=2){
        return true;
        }else{
        return false;
    }
}
